#!/bin/bash

NOW=$(TZ=America/Los_Angeles date)

> generated.tex

echo "\\renewcommand{\\genDate}{$NOW}" >> generated.tex
