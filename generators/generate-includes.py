#!/usr/bin/env python3
import os
from enum import Enum
from queue import PriorityQueue

import click

class ContentLevel(Enum):
    TOP_LEVEL = 0
    SECONDARY_LEVEL = 1
    TERTIARY_LEVEL = 2
    QUATERNARY_LEVEL = 3

LEVEL_TO_NAME= {
    ContentLevel.TOP_LEVEL: "part",
    ContentLevel.SECONDARY_LEVEL: "section",
    ContentLevel.TERTIARY_LEVEL: "subsection",
    ContentLevel.TERTIARY_LEVEL: "subsubsection"
}

def add_content_to_includes(out_file, source_filepath: str, level: ContentLevel):
    output_filename = out_file.name
    relative_path = os.path.splitext(os.path.relpath(source_filepath, os.path.dirname(output_filename)))[0]

    out_file.write("\\input{{{relpath}}}\n\\newpage\n".format(relpath=relative_path))
    

def add_group_to_toc(out_file, group: str, level: ContentLevel):
    out_file.write("\\addtocitem{{{type}}}{{\\titleFont{{\\MakeUppercase{{{name}}}}}}}\n"
            .format(type=LEVEL_TO_NAME[level], name=group))

def add_group_to_includes(out_file, group: str, source_filepath: str,
                          level: ContentLevel, suffix: str = None,
                          num_items: int = 0):
    group_text = "{}{}".format(group, suffix)

    # Add this group to the includes as a new "title" page
    if num_items % 8 == 7:
        out_file.write("\\addtocontents{toc}{\\protect\\newpage}\n")
        num_items += 2

    add_group_to_toc(out_file, group_text, level)

    sub_directories = PriorityQueue()
    inputs = PriorityQueue()
    for under in os.listdir(source_filepath):
        new_path = os.path.join(source_filepath, under)

        if os.path.isdir(new_path):
            sub_directories.put(new_path)
        else:
            inputs.put(new_path)

    if group:
        out_file.write("\\addsectionpage{{{text}}}\n".format(text=group_text))

    next_level = ContentLevel(level.value + 1)

    item_count = num_items
    while not inputs.empty():
        if item_count and item_count % 8 == 0:
            out_file.write("\\addtocontents{toc}{\\protect\\newpage}\n")
        add_content_to_includes(out_file, inputs.get(), next_level)
        item_count += 1

    out_file.write("\n")

    for directory in sub_directories.queue:
        item_count = add_group_to_includes(out_file, os.path.basename(directory).capitalize(), directory,
                                           ContentLevel.TOP_LEVEL, suffix, item_count)

    return item_count



@click.command()
@click.option("-s", "--source-dir", required=True,
              help="The top level directory of the contents to generate into an includes.tex file")
@click.option("-o", "--output-file", required=True,
              help="The file to output the includes to.")
@click.option("-t", "--top-level-alias", type=str, default="",
              help="An alias for the top level directory in the TOC")
@click.option("-x", "--section-suffix", type=str, default="",
              help="A suffix to add to each section title")
def generate_includes(source_dir: str, output_file: str, top_level_alias: str, section_suffix: str):
    if not os.path.exists(source_dir) or not os.path.isdir(source_dir):
        raise ValueError("Provided source dir {} either does not exist or is not a directory.".format(source_dir))
    with open(output_file, "w") as out_file:
        add_group_to_includes(out_file, top_level_alias, source_dir, ContentLevel.TOP_LEVEL, section_suffix)

if __name__ == "__main__":
    generate_includes()
