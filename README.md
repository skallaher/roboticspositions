# Contents

## Interviews
Files used to generate an interview process procedure PDF.

[Latest Master Version](https://gitlab.com/skallaher/roboticspositions/-/jobs/artifacts/master/browse?job=build_interview_protocols)

## Listings
Files used to generate a position listings PDF.

[Latest Master Version](https://gitlab.com/skallaher/roboticspositions/-/jobs/artifacts/master/browse?job=build_listings)

## Resources
General large file resources used by other tools.


### Note
If any of the links above are not found, the build may not have run successfully or the build is over a week old.

[![build status](https://gitlab.com/skallaher/roboticspositions/badges/master/pipeline.svg)](https://gitlab.com/skallaher/roboticspositions/-/pipelines)

If the above is not green, that means that the build has failed. Otherwise, it is possible that the artifacts have expired and the pipeline just needs to be rerun.

