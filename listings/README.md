Listings
===

This directory contains the LaTeX files used to generate the position listings PDF.


Building
===

To build the listings PDF, run `make`. This will place the generated PDF in the `build` directory named `positions.pdf`.


Files
===

The following LaTeX files are contained here:

- `frontpage.tex` - Manages the front "splash" page of the PDF.
- `generated.tex` - Autogenerated values used within the PDF. Do NOT modify.
- `glossary.tex`  - Contains values which are added to the glossary and can be referenced elsewhere. (See [Adding to the Glossary](#adding-to-the-glossary) and [Referencing the Glossary](#referencing-the-glossary))
- `includes.tex`  - Autogenerated file containing roles as they should appear in the Table of Contents. Do NOT modify.
- `main.tex`      - The main LaTeX file. Do NOT modify if you are unfamiliar with LaTeX.
- `variables.tex` - Globally defined variables for things such as text/background colors and common text.

- `roles` directory   - Contains all of the descriptions of each role. These are ordered alphabetically in the Table of Contents. Subdirectories are used for different categories in the Table of Contents. (See [Modifying a Role](#modifying-a-role) and [Adding a New Role](#adding-a-new-role)

The following scripts are contained here:

- `generate-includes.sh`  - Creates the `includes.tex` file.
- `generate-variables.sh` - Creates the `generated.tex` file.


Modifying a Role
===

To modify a role, open its corresponding file in the `roles` directory to make changes.


Adding a New Role
===

To add a new role, create a new file named with the format of `<role name in CamelCase>.tex` and add a reference to it in the `includes.tex` file.

[Example Below](#example-role-file)

Within this file, there are a few sections:
- Header
- Who (whosection)
- What (whatsection)
- Requirements (reqsection)
- Desired Skills (desiredsection)

### Header
This is a required line within the role file
```tex
\begin{Position}{<role name>}{<role this reports to>}
```

### Who

The format of the Who section is as follows:
```tex
\whosection{
% Text describing who the person filling this position is. Preferably a short sentance.
}
```

### What

The format of the What section is as follows:
```tex
\whatsection{
% Text describing what this individual is responsible for, typically done with a list as follows
	\begin{itemize}
		\item This is a single item in a list
	\end{itemize}.
}
```

### Requirements

The format of the Requirements section is as follows:
```tex
\reqssection{
% Text describing the required qualifications in order to hold this position, typically done with a list as follows 
	\begin{itemize}
		\item This is a single item in a list
	\end{itemize}.
}
```

### Desired Skills

The format of the Desired Skills section is as follows:
```tex
\reqssection{
% Text describing the desired qualifications for those in this position, typically done with a list as follows 
	\begin{itemize}
		\item This is a single item in a list
	\end{itemize}.
}
```

### Example Role File
```tex
\begin{Position}{Example}{ExampleReport}

\whosection{
This is an example of a role description.
}

\whatsection{
This example role does some things, here they are:
	\begin{itemize}
		\item Some cool stuff.
		\item Even more cool stuff.
		\item Some not so cool, but really important stuff.
	\end{itemize}
}

\reqssection{
	\begin{itemize}
		\item At least one cool story.
		\item A good joke.
	\end{itemize}
}

\desiredsection{
	\begin{itemize}
		\item Can recite Pi to 20 digits.
	\end{itemize}
}
```

### Adding to `includes.tex`

To provide your new role so that it will be added to the final PDF, add new lines in the `includes.tex` file of the following format:

```tex
\input{<path to the file, excluding the .tex extension>}
\newpage
```

This should be placed after the line (`\addtocpart{<section>}`) defining which section of the table of contents this should be added to.


Adding to the Glossary
===

To add a new entry to the glossary, add the following to the end of the `glossary.tex` file:

```tex
\newglossaryentry{<entry key>}{
	name={<human readable name>},
	plural={<plural human readable name>},
	description={<Description shown in glossary>}
}
```

Referencing the Glossary
===

To reference a glossary entry within the text, replace the entry value with:

```tex
\gls{<entry key>}    % Singular version
\glspl{<entry key>}  % Plural version
```
