#!/usr/bin/env bash

SOURCE_DIR=$(realpath ./roles)
TARGET_FILE=$(realpath ./includes.tex)

pushd ../generators > /dev/null

if [[ ! -d .generator-env ]]; then
    virtualenv -p python3 .generator-env
    source .generator-env/bin/activate
    pip install -r requirements.txt
else
    source .generator-env/bin/activate
fi

./generate-includes.py -s "$SOURCE_DIR" -o "$TARGET_FILE" -t "Executive" -x " Roles"

deactivate
